const UglifyJS = require("uglify-js");
const fs = require("fs");

let generate = (c) => {
    return new Promise((resolve, reject) => {
        const filename = require.resolve("./react.js");

        let output = "";
        const readStream = fs.createReadStream(filename);

        readStream.on('data', function(chunk) {
            output += chunk.toString('utf8');
        });

        readStream.on('end', function() {
            const swc = require("@swc/core");
            swc
                .transform(c, {
                    sourceMaps: true,
                    // Input files are treated as module by default.
                    isModule: false,

                    // All options below can be configured via .swcrc
                    jsc: {
                        parser: {
                            syntax: "ecmascript",
                            jsx: true
                        },
                        transform: {},
                    },
                })
                .then((o) => {
                    resolve({ code: UglifyJS.minify(output + "\n" + o.code).code + `\n`, map: o.map });
                });
        });
    });
}

module.exports = { generate };