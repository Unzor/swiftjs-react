const parser = require("node-html-parser");
const fs = require('fs');

const {
    generate
} = require('./jsx');

function loop(a, f) {
    return new Promise((resolve, reject) => {
        var i = 0;
        let next = () => {
            if (a[i]) {
                f(a[i], i++, next);
            } else {
                resolve(true);
            }
        };
        f(a[i], i++, next);
    });
}

let migrate = (dirname, out) => {
    let dir = fs.readdirSync(dirname);
    loop(dir, async function(file, _i, next) {
        if (fs.statSync(dirname + '/' + file).isFile() && file.endsWith('.html')) {
            let source = fs.readFileSync(dirname + '/' + file).toString();
            let f = parser.parse(source);
            await loop(f.querySelectorAll('script'), async (x, _i, next) => {
                if (x.rawAttrs.includes('src')) {
                    let attrsToArr = (c) => {
                        var s = c.split("\"");
                        var out = [""];
                        var i = 0;

                        if (s[s.length - 1].length === 0) s = s.slice(0, -1);
                        s.forEach(function(e) {
                            if (!e.includes("=")) {
                                out[i] += "\"" + e + "\"";
                            } else {
                                if (!i) {
                                    out[0] += e;
                                    out[1] += e;
                                    i += 1;
                                } else {
                                    out.push(e);
                                    i += 1;
                                }
                            }
                        })
                        out = out.slice(1);
                        out[0] = out[0].slice(9);
                        out = out.map(x => x.startsWith(" ") ? x.slice(1) : x);
                        return out;
                    }

                    function arrayFindIncludes(n, r) {
                        var u = [];
                        return r.forEach(function(r) {
                            r.includes(n) && u.push(r)
                        }), u
                    }
                    let arrs = attrsToArr(x.rawAttrs);
                    let arr = arrayFindIncludes('src', arrs);
                    let source1 = arr.map(x => x.slice(5, -1));
                    source = source.replace(x.outerHTML, '');
                    f.querySelector('body').setAttribute('onload', `(async () => {await emit('${source1[0].split('/').pop().split('.').shift()}')})()`);
                    source = f.outerHTML;
                    let template = `---
${source1.map(x => `await Swift.runOnEvent("default/${x.split('/').pop()}", "${x.split('/').pop().split('.')[0]}")`)}
Swift.finish();
---
${source}`;
                    if (!fs.existsSync(out)) {
                        fs.mkdirSync(out);
                        fs.mkdirSync(`${out}/chunks`);
                        fs.mkdirSync(`${out}/render`);
                        fs.mkdirSync(`${out}/render/default`);
                        fs.writeFileSync(`${out}/render/worker.js`, `importScripts('https://cdn.jsdelivr.net/npm/comlinkjs@2.3/comlink.global.min.js');

class Fetch {
    conoutuctor() {
        this._baseUrl = "";
        this._defaultHeaders = {};
        this._defaultBody = {};
    }

    getBaseUrl() {
        return this._baseUrl;
    }

    getDefaultHeaders() {
        return this._defaultHeaders;
    }

    getDefaultBody() {
        return this._defaultBody;
    }

    setBaseUrl(baseUrl) {
        this._baseUrl = baseUrl;
    }

    setDefaultHeaders(defaultHeaders) {
        this._defaultHeaders = defaultHeaders;
    }

    setDefaultBody(defaultBody) {
        this._defaultBody = defaultBody;
    }

    async get(endpoint = '') {
        const response = await fetch(endpoint)
        const data = await response.arrayBuffer();
        return data;
    }

    async post(endpoint = '', body = undefined, headers = {}) {
        this._send('POST', endpoint, body, headers);
    }

    async put(endpoint = '', body = undefined, headers = {}) {
        this._send('PUT', endpoint, body, headers);
	}
	
    async delete(endpoint = '', body = undefined, headers = {}) {
        this._send('DELETE', endpoint, body, headers);
    }

    async _send(method, endpoint = '', body = undefined, headers = {}) {
        const response = await fetch(this.getBaseUrl() + endpoint, {
            method,
            headers: { ...this.getDefaultHeaders(), ...headers },
            body: JSON.stringify({ ...this.getDefaultBody(), ...body })
        })
        const data = await response.arrayBuffer();
        return data;
    }
}

Comlink.expose({ Fetch }, self);`);
                    }
                    fs.writeFileSync(out + '/render/' + file.split('.').slice(0, -1).join('.') + '.swft', template);
                    const r = await generate(fs.readFileSync(`${dirname}/${source1[0]}`).toString());
                    fs.writeFileSync(`${out}/render/default/${source1[0].split('/').pop()}`, r.code + `//# sourceMappingURL=default/${source1[0].split('/').pop()}.map`);
                    fs.writeFileSync(`${out}/render/default/${source1[0].split('/').pop()}.map`, r.map);
                    next();
                }
            })
        }
        next();
    });
};

module.exports = migrate;