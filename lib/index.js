#!/usr/bin/env node
#!/usr/bin/env node
const { Command } = require('commander');
const transform = require('./generate');
const program = new Command();

program
  .name('swiftjs-react')
  .description('A React to SwiftJS transformation tool')
  .version('1.0.0');

program.command('transform')
  .description('Transforms a React project into a SwiftJS project.')
  .argument('<input>', 'directory to transform into a SwiftJS project')
  .argument('<output>', 'output directory')
  .action((input, output) => {
	transform(input, output);
  });

program.parse();