# SwiftJS (React)
A tool to transform a React project into a SwiftJS project.

# Usage
```
swift-react transform input_directory output
```
input_directory:
```
\---input
	index.jsx 
	index.html
```

# Help
```
Usage: swiftjs-react [options] [command]

A React to SwiftJS transformation tool

Options:
   -V, --version               output the version number
   -h, --help                  display help for command
  
Commands:
  transform <input> <output>  Transforms a React project into a SwiftJS project.
  help [command]              display help for command
```